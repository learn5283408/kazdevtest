<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return view('posts.index',
            [
                'posts' => Post::latest()->filter(request(['tag', 'search']))->paginate(5)->withQueryString(),
                'tags'  => Tag::all()
            ]
        );
    }

    public function show(Post $post)
    {
        return view('posts.show',
            [
                'post' => $post,
                'tags' => Tag::all(),
            ]);
    }

}
