<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $expiresAt = now()->addHour();

            if ($user->is_admin) {
                $user->tokens()->delete();
                $token = $user->createToken('Laravel', expiresAt: $expiresAt)->plainTextToken;
                $token = explode('|', $token);

                return response()->json([
                    'success' => true,
                    'token'   => $token[1],
                    'expires_at' => $expiresAt->format('d.m.Y | H:i'),
                    'user'    => $user
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Access forbidden'
                ], 403);
            }

        }
        return response()->json([
            'success' => false,
            'message' => 'Unauthorized'
        ], 401);
    }
}
