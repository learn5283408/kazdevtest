<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    public function destroy()
    {
        auth()->logout();
        return redirect('/')->with('success', 'Вы вышли из аккаунта');
    }

    public function create()
    {
        return view('sessions.create');
    }

    /**
     * @throws ValidationException
     */
    public function store()
    {
        $attributes = request()->validate(
            [
                'email'    => ['required', 'email'],
                'password' => ['required']
            ]
        );

        if (auth()->attempt($attributes)) {
            session()->regenerate();

            return redirect('/')->with('success', 'Добро пожаловать ' . auth()->user()->name);
        }

        throw ValidationException::withMessages(['email' => 'Email или пароль не совпадают']);

    }

}
