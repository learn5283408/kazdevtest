<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    public function index()
    {
        return view('posts.admin.index',
            [
                'posts' => Post::latest()->get()
            ]);
    }

    public function create()
    {
        return view('posts.admin.create', ['tags' => Tag::all()]);
    }

    public function store()
    {
        $attributes = request()->validate(
            [
                'title'        => ['required', 'min:2', 'max:256'],
                'image'        => ['required', 'image', 'max:3000', 'dimensions:max_width=901,max_height=401'],
                'video'        => ['required', 'min:2', 'max:256'],
                'introduction' => ['required', 'min:2', 'max:1000'],
                'body'         => ['required', 'min:2', 'max:1000'],
                'tags.*'       => [Rule::exists('tags', 'id')]
            ]
        );

        $attributes['image'] = request()->file('image')->store('images');

        $post = Post::create(
            [
                'user_id'      => request()->user()->id,
                'title'        => $attributes['title'],
                'image'        => $attributes['image'],
                'video'        => $attributes['video'],
                'introduction' => $attributes['introduction'],
                'body'         => $attributes['body']
            ]
        );

        $post->tag()->attach($attributes['tags']);

        return redirect('/')->with('success', 'Пост создан');
    }

    public function edit(Post $post)
    {
        return view('posts.admin.edit',
            [
                'tags' => Tag::all(),
                'post' => $post
            ]);
    }

    public function update(Post $post)
    {
        $attributes = request()->validate(
            [
                'title'        => ['required', 'min:2', 'max:256'],
                'image'        => ['image', 'max:3000', 'dimensions:max_width=901,max_height=401'],
                'video'        => ['required', 'min:2', 'max:256'],
                'introduction' => ['required', 'min:2', 'max:1000'],
                'body'         => ['required', 'min:2', 'max:1000'],
                'tags.*'       => [Rule::exists('tags', 'id')]
            ]
        );

        if (isset($attributes['image'])) {
            $attributes['image'] = request()->file('image')->store('images');
        }

        $post->tag()->sync($attributes['tags']);
        unset($attributes['tags']);

        $post->update($attributes);

        return redirect('/')->with('success', 'Пост был отредактирован');
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return back()->with('success', 'Пост был удален');
    }
}
