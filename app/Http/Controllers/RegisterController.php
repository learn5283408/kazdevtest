<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    public function create()
    {
        return view('register.create');
    }

    public function store()
    {
        $messages = [
            'name.required'                  => 'Имя обязательно для заполнения.',
            'name.max'                       => 'Имя должно быть не более :max символов.',
            'name.min'                       => 'Имя должно быть не меньше :min символов.',
            'email.required'                 => 'Email адрес обязателен для заполнения.',
            'email.max'                      => 'Email адрес должен быть не более :max символов.',
            'email.min'                      => 'Email адрес должен быть не менее :min символов.',
            'email.unique'                   => 'Пользователь с таким email уже есть',
            'password.required'              => 'Пароль обязателен для заполнения.',
            'password.max'                   => 'Пароль должен быть не более :max символов.',
            'password.confirmed'             => 'Введенные вами пароли не совпадают',
            'password_confirmation.required' => 'Пароль обязателен для заполнения.'
        ];


        $attributes = request()->validate(
            [
                'name'                  => ['required', 'max:256', 'min:2'],
                'email'                 => ['required', 'email', 'max:256', 'min:2', Rule::unique('users', 'email')],
                'password'              => ['required', 'max:256', 'min:2', 'confirmed'],
                'password_confirmation' => ['required']
            ], $messages
        );

        $user = User::create($attributes);

        auth()->login($user);

        return redirect('/')->with('success', 'Аккаунт зарегистрирован');

    }
}
