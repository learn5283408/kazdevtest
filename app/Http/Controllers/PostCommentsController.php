<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostCommentsController extends Controller
{
    public function store(Post $post)
    {
        $messages = [
            'body.required' => 'Поле для комментирования обязательно к заполнению',
            'body.min'      => 'Минимум от 2 символов и более',
        ];

        request()->validate(
            [
                'body' => ['required', 'min:2']
            ], $messages
        );

        $post->comments()->create(
            [
                'user_id' => auth()->id(),
                'body'    => request('body')
            ]);

        return back()->with('success', 'Ваш комментарии опубликован');
    }
}
