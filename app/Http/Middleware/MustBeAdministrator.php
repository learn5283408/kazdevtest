<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class MustBeAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     * @return ResponseAlias
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->user()->cannot('admin')){
            abort(ResponseAlias::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
