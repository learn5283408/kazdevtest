FROM php:8.2-fpm-alpine

# Set working directory
WORKDIR /var/www/testkz

# Install Additional dependencies
RUN apk update && apk add --no-cache \
    bash \
    build-base shadow supervisor \
    libxml2-dev \
    libpng-dev \
    libwebp-dev \
    libxpm-dev \
    libmcrypt-dev \
    autoconf \
    zip \
    git \
    nano \
    nodejs \
    npm \
    busybox-extras

RUN apk add --no-cache --virtual .persistent-deps \
                libldap
RUN apk update && \
         apk add --no-cache --virtual .build-deps $PHPIZE_DEPS openldap-dev && \
         docker-php-ext-install ldap && \
         apk del .build-deps

# Add and Enable PHP-PDO Extenstions

RUN pecl install redis
#RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-enable pdo_mysql
RUN docker-php-ext-install pcntl
RUN docker-php-ext-install gd
RUN docker-php-ext-enable redis

# Remove Cache
RUN rm -rf /var/cache/apk/*


# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# Use the default production configuration ($PHP_INI_DIR variable already set by the default image)
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Add UID '1000' to www-data
RUN usermod -u 1000 www-data

COPY . .

RUN chmod 777 -R storage
RUN chown -R www-data:www-data .

USER www-data

RUN composer install
