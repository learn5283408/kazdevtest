import Alpine from 'alpinejs';
window.Alpine = Alpine;

import 'bootstrap';
import '@popperjs/core';

Alpine.start();
