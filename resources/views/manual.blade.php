<x-head></x-head>
<x-body>
    <x-navbar/>
    <div class="container mt-5">

       <h1 class="text-center">ROADMAP Manual QA <span class="text-success">(in progress)</span></h1>

        <div class="container mb-5">
            <div class="row">
                <div class="col-lg-7 mx-auto">

                    <ul class="timeline">
                        <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                            <div class="timeline-arrow"></div>
                            <a href="https://testkz.com/posts/2" target="_blank" class="text-decoration-none"><h2 class="h5 mb-0">Знакомство с курсом</h2></a>
                            <span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>26 октября, 2024</span>
                                <p class="text-small mt-2 font-weight-light">Знакомство с курсом, все о предстоящем обучении.</p>
                        </li>
                        <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                            <div class="timeline-arrow"></div>
                            <a href="https://testkz.com/posts/4" target="_blank" class="text-decoration-none"><h2 class="h5 mb-0">Теория тестирования | Часть 0</h2></a><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>3 октября, 2024</span>
                            <p class="text-small mt-2 font-weight-light">Что такое тестирование | Зачем тестировать | Цели тестирования</p>
                        </li>
                        <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                            <div class="timeline-arrow"></div>
                            <a href="https://testkz.com/posts/5" target="_blank" class="text-decoration-none"><h2 class="h5 mb-0">Теория тестирования | Часть 1</h2></a><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>13 октября, 2024</span>
                            <p class="text-small mt-2 font-weight-light">SDLC | Жизненный цикл разработки ПО | Уровни и виды тестирования</p>
                        </li>
                        <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                            <div class="timeline-arrow"></div>
                            <h2 class="h5 mb-0">Знакомство с Linux, сервисы менеджмента</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>неизвестно</span>
                            <p class="text-small mt-2 font-weight-light">В процессе</p>
                        </li>

                        <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                            <div class="timeline-arrow"></div>
                            <h2 class="h5 mb-0">Знакомство с git и gitlab, работа с SSH</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>неизвестно</span>
                            <p class="text-small mt-2 font-weight-light">В процессе</p>
                        </li>

                        <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                            <div class="timeline-arrow"></div>
                            <h2 class="h5 mb-0">Тестирование API. Работа с БД</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>неизвестно</span>
                            <p class="text-small mt-2 font-weight-light">В процессе</p>
                        </li>
                    </ul>

                </div>
            </div>
        </div>

    </div>
</x-body>
