<x-head/>
<x-body>
<x-navbar/>

<!-- Page content-->
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-lg-8">
            <!-- Post content-->
            <article>
                <!-- Post header-->
                <header class="mb-4">
                    <!-- Post title-->
                    <h1 class="fw-bolder mb-1">{{$post->title}}</h1>
                    <!-- Post meta content-->
                    <div class="text-muted fst-italic mb-2">Опубликовано {{$post['created_at']->diffForHumans()}}</div>
                    <!-- Post tags-->
                    @foreach($post->tag as $tag)
                        <a class="mb-2 badge bg-secondary text-decoration-none link-light" href="/?tag={{$tag->slug}}">{{$tag->name}}</a>
                    @endforeach

                    <!-- Preview video iframe-->
                    <div class="embed-responsive embed-responsive-16by9 mb-4">
                        <iframe class="embed-responsive-item w-100" src="{{$post->video}}" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style="height: 50vh;"></iframe>
                    </div>

                @if(isset($post->introduction))
                    <section class="mb-3 mt-3 border border-5 rounded border-success-subtle">
                        <p class="fs-5 m-2 text-center">
                            <b>Краткая вводная, что вы узнали сегодня нового:</b>
                        </p>
                        <ul>
                            @foreach(explode(PHP_EOL, $post->introduction) as $word)
                            <li>{{$word}}</li>
                            @endforeach
                        </ul>
                    </section>
                    @endif

                    <section class="mb-2">
                        @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                        <p class="fs-5">
                            {{$paragraph}}
                        </p>
                        @endforeach
                    </section>
                </header>
            </article>

            <div class="col-lg-12 mb-3">
                <div class="d-flex justify-content-end">
                    <div>
                        <span class="text-muted fw-bolder">Автор: </span>
                        <span class="text-success fw-bold">{{$post->author->name}}</span>
                    </div>
                </div>
            </div>
            <x-comments :post="$post"/>

        </div>

        <x-search :tags="$tags"/>

    </div>

    <a href="{{URL::previous()}}" class="text-decoration-none fs-5">Вернуться назад</a>

</div>
    @if(session()->has('success'))
        <div x-data="{show: true}"
             x-init="setTimeout(() => show = false, 3000)"
             x-show="show" x-transition
             class="position-fixed bottom-0 end-0 m-5 border border-dark rounded-pill bg-success bg-opacity-75">
            <p class="text-center m-2 fw-bold">{{session('success')}}</p>
        </div>
    @endif
</x-body>
