<x-head/>
<x-body>
<x-navbar/>

<!-- Page content-->
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-lg-8">
            <!-- Post content-->
            <article>
                <!-- Post header-->
                <header class="mb-4">
                    <!-- Post title-->
                    @if ($posts->count())
                        @foreach($posts as $post)
                    <a href="/posts/{{$post->id}}" class="text-decoration-none link"><h2 class="fw-bolder mb-1">{{$post->title}}</h2></a>
                    <!-- Post meta content-->
                    <div class="text-muted fst-italic mb-2">Опубликовано {{$post['created_at']->diffForHumans()}}</div>
                    <!-- Post tags-->
                        @foreach($post->tag as $tag)
                                <a class="mb-2 badge bg-secondary text-decoration-none link-light" href="/?tag={{$tag->slug}}">{{$tag->name}}</a>
                        @endforeach
                            <!-- Preview image figure-->
                    <figure class="mb-4"><img class="img-fluid rounded" src="{{asset('storage/' . $post->image)}}" alt="..." /></figure>
                            <hr class="border border-primary border-3 opacity-7">
                        @endforeach
                    {{$posts->links()}}
                    @else
                    <h1 class="fw-bolder mt-1">Oops! Новостей по вашему поиску нет!</h1>
                    @endif
                </header>
            </article>
        </div>
<x-search :tags="$tags"/>

    </div>
</div>

    @if(session()->has('success'))
        <div x-data="{show: true}"
             x-init="setTimeout(() => show = false, 3000)"
             x-show="show" x-transition
            class="position-fixed bottom-0 end-0 m-5 border border-dark rounded-pill bg-success bg-opacity-75">
            <p class="text-center m-2 fw-bold">{{session('success')}}</p>
        </div>
    @endif

</x-body>
