<x-head/>
<x-body>
    <x-navbar/>

    <!-- Page content-->
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-lg-12">
                <!-- Post content-->
                <article>
                    <!-- Post header-->
                    <header class="mb-5">
                        <h1 class="text-center">Список постов</h1>
                    </header>

                    @if($posts->count())
                    <div class="container">
                        @foreach($posts as $post)

                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="d-flex flex-column flex-lg-row">
                                    <div class="row flex-fill">
                                        <div class="col-sm-3">
                                            <a class="text-decoration-none link-dark" href="/posts/{{$post->id}}"><h4 class="h5">{{$post->title}}</h4></a>
                                            <span class="badge bg-success">Опубликовано: {{$post['created_at']->diffForHumans()}}</span>
                                        </div>

                                        <div class="col-sm-2 py-2 ms-3">
                                            <span class="badge bg-info">{{$post->author->name}}</span>
                                        </div>

                                        <div class="col-sm-3 py-2">
                                            @foreach($post->tag as $tag)
                                            <span class="badge bg-secondary">{{$tag->name}}</span>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-3 text-lg-end">
                                            <div class="d-flex justify-content-end">
                                                <a href="/admin/posts/{{$post->id}}/edit" class="btn btn-primary me-2">Edit</a>

                                                <form action="/admin/posts/{{$post->id}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                            @else
                                <h1 class="fw-bolder mt-1 text-center">Oops! Здесь пусто!</h1>
                            @endif

                    </div>
                </article>
            </div>
        </div>
    </div>

    @if(session()->has('success'))
        <div x-data="{show: true}"
             x-init="setTimeout(() => show = false, 3000)"
             x-show="show" x-transition
             class="position-fixed bottom-0 end-0 m-5 border border-dark rounded-pill bg-success bg-opacity-75">
            <p class="text-center m-2 fw-bold">{{session('success')}}</p>
        </div>
    @endif
</x-body>
