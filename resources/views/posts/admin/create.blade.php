<x-head/>
<x-body>
    <x-navbar/>

    <!-- Page content-->
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-lg-12">
                <!-- Post content-->
                <article>
                    <!-- Post header-->
                    <header class="mb-5">
                        <h1 class="text-center">Новый пост</h1>
                    </header>

                    <form action="/admin/posts" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="mb-3 w-75 m-auto">
                            <label for="title" class="fw-bold">Заголовок</label>
                            <input type="text" value="{{old('title')}}" name="title" class="form-control" id="title" placeholder="Заголовок" required>

                            <label for="image" class="mt-5 fw-bold">Картинка (будет отображаться только в preview, размеры не больше 900x400)</label>
                            <input type="file" name="image" class="form-control" id="image" required accept="image/*">

                            <label for="video" class="mt-5 fw-bold">Ссылка на видео</label>
                            <input type="text" name="video" class="form-control" placeholder="Копировать ссылку из Youtube вкладки 'Поделиться'" id="video" value="{{old('video')}}">

                            <label for="introduction" class="mt-5 fw-bold">Краткая вводная, что вы узнаете сегодня нового:</label>
                            <textarea name="introduction" class="form-control" id="introduction" placeholder="Список создается через новую строку" cols="30" rows="5" required>{{old('introduction')}}</textarea>

                            <label for="body" class="mt-5 fw-bold">Основное описание</label>
                            <textarea name="body" class="form-control" id="body" placeholder="Новый параграф создается через новую строку" cols="30" rows="5" required>{{old('body')}}</textarea>

                            <label class="mt-5 fw-bold">Выберите теги: </label>
                            @foreach($tags as $tag)
                                <div class="form-check form-check-inline m-2">
                                    <input class="form-check-input" type="checkbox" name="tags[]" id="{{$tag->slug}}" value="{{$tag->id}}" {{ in_array($tag->id, old('tags', [])) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="{{$tag->slug}}">{{$tag->name}}</label>
                                </div>
                            @endforeach

                            @foreach($errors->all() as $error)
                                <p class="text-danger mt-3">{{$error}}</p>
                            @endforeach
                            <div>
                            <button type="submit" class="btn btn-success mt-5">Создать пост</button>
                            </div>
                        </div>

                    </form>

                </article>
            </div>
        </div>
    </div>
</x-body>
