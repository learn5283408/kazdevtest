@props(['tags'])
<div class="col-lg-4">
    <!-- Search widget-->
    <div class="card mb-4">
        <div class="card-header">Найти</div>
        <div class="card-body">
            <form method="GET" action="/">
                <div class="input-group">
                    <input class="form-control" name="search" type="text" placeholder="Поиск..." value="{{request('search')}}"
                           aria-label="Enter search term..." aria-describedby="button-search"/>
                    <button class="btn btn-primary" id="button-search" type="submit">Поиск</button>
                </div>
            </form>
        </div>
    </div>

    <x-tags :tags="$tags"/>
    <x-widget/>
</div>
