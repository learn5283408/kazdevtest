@props(['tags'])
<div class="card mb-4">
    <div class="card-header">Теги</div>
    <div class="card-body">
        <div class="row">
            @foreach($tags as $tag)
            <div class="col-sm-6">
                <ul class="list-unstyled mb-0">
                    <li><a href="/?tag={{$tag->slug}}" class="text-decoration-none">{{$tag->name}}</a></li>
                </ul>
            </div>
            @endforeach
        </div>
    </div>
</div>
