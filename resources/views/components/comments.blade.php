@props(['post'])
<section class="mb-5">
    <div class="card bg-light">
        <div class="card-body">
            <!-- Comment form-->
            <form class="mb-4" method="post" action="/posts/{{$post->id}}/comments">
                @csrf
                    <textarea name="body" class="form-control" rows="3" placeholder="Оставляй свои комментарии ...">{{old('body')}}</textarea>
                <button type="submit" class="mt-3 btn btn-primary">Комментировать</button>
                @foreach($errors->all() as $error)
                    <p class="text-danger mt-3">{{$error}}</p>
                @endforeach
            </form>
            <hr>

            @if($post->comments->count())
                @foreach($post->comments->sortByDesc('created_at') as $comment)

                    <div class="d-flex mb-4">
                        <!-- Parent comment-->
                        <div class="flex-shrink-0"><img class="rounded-circle" src="https://i.pravatar.cc/50?u={{$comment->author->name}}" alt="..." /></div>
                        <div class="ms-3 d-flex align-items-center justify-content-between w-100">
                            <div>
                                <div class="fw-bold text-success">{{$comment->author->name}}</div>
                            </div>
                            <div class="text-black fw-bold ms-auto">Опубликовано: {{$comment['created_at']->diffForHumans()}}</div>
                        </div>
                    </div>
                    <p>{{$comment->body}}</p>
                    <hr>

                @endforeach
            @endif

        </div>
    </div>
</section>
