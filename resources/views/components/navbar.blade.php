@php
    use Illuminate\Support\Facades\Request;
@endphp
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/">TESTKZ</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item"><a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="/">Главная</a></li>
                <li class="nav-item"><a class="nav-link {{ Request::is('manual') ? 'active' : '' }}" href="/manual">Roadmap Manual</a></li>
                <li class="nav-item"><a class="nav-link {{ Request::is('automation') ? 'active' : '' }}" href="automation">Roadmap Automation</a></li>
                <li class="nav-item"><a class="nav-link {{ Request::is('contacts') ? 'active' : '' }}" href="/contacts">Контакты</a></li>
                @auth

                    <div class="btn-group">
                        <button type="button" class="btn btn-outline-info dropdown-toggle" data-bs-toggle="dropdown">
                            Привет {{auth()->user()->name}}!
                        </button>
                        <div class="dropdown-menu">
                            @can('admin')
                            <a class="nav-link text-primary" href="/admin/posts/">Список постов</a>
                            <div class="dropdown-divider"></div>
                            <a class="nav-link text-primary" href="/admin/posts/create">Создать пост</a>
                            <div class="dropdown-divider"></div>
                            @endcan
                                <form method="post" action="/logout">
                                @csrf
                                <button class="nav-link text-danger" type="submit">Выйти</button>
                            </form>
                        </div>
                    </div>

                @else
                    <li class="nav-item"><a class="nav-link {{ Request::is('login') ? 'active' : '' }}" href="/login">Авторизация</a></li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
