<x-head></x-head>
<x-body>
    <x-navbar></x-navbar>
    <div class="container mt-5">

        <div class="img-thumbnail text-center border-0">
            <img src="{{asset('img/auth-icon.png')}}" alt="auth-icon" width="25%">
        </div>

        <form method="post" action="/login">
            @csrf
            <div class="mb-3 w-25 m-auto">
                <label for="exampleInputEmail1" class="mt-2">Email адрес</label>
                <input type="email" value="{{old('email')}}" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="example@mail.com">
                @error('email')
                <p class="text-danger mt-3">{{$message}}</p>
                @enderror
                <label for="password" class="mt-2">Пароль</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Ваш пароль">
                @error('password')
                <p class="text-danger mt-3">{{$message}}</p>
                @enderror
            <button type="submit" class="btn btn-primary mt-3">Войти</button>
            </div>
        </form>

        <div class="mb-3 w-25 m-auto">
            <p>Впервые здесь? Тогда регистрируйся</p>
            <a href="/register">Зарегистрироваться</a>
        </div>

    </div>
</x-body>
