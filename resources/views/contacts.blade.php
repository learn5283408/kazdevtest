<x-head></x-head>
<x-body>
    <x-navbar></x-navbar>
    <div class="container mt-5">

        <div class="img-thumbnail text-center border-0">
            <img src="{{asset('img/feedback.png')}}" alt="auth-icon">
        </div>

        <div class="mb-3 mt-5 w-100 m-auto">
            <p class="fs-5">Приветствую вас на моем сайте! Меня зовут <b>Абай</b> и я занимаюсь обучением <b>тестированию программного обеспечения</b>.</p>
            <p class="fs-5">На этом сайте вы найдете полезные статьи, уроки и ресурсы, которые помогут вам освоить эту сферу направления.</p>
            <p class="fs-5">Если у вас возникли вопросы или вы хотите связаться со мной, не стесняйтесь обращаться через мои профили в социальных сетях:</p>
            <p><b>Telegram: <a href="https://t.me/mylable" target="_blank">Send me message</a></b></p>
            <p><b>Discord группа: <a href="https://discord.gg/AAAyYSFGd6" target="_blank">TEST-KZ-COM</a></b></p>
            <br>
{{--            <p class="fs-5">Или оставьте свое сообщение через обратную связь что ниже</p>--}}
{{--            <form action="/message" method="post" class="w-25">--}}
{{--                <div class="mb-3">--}}
{{--                <label for="name"><b>Имя:</b></label><br>--}}
{{--                <input type="text" id="name" name="name" class="form-control" required><br>--}}
{{--                <label for="email"><b>Ваш Email для связи с вами:</b></label><br>--}}
{{--                <input type="email" id="email" name="email" class="form-control" required><br>--}}
{{--                <label for="message"><b>Сообщение:</b></label><br>--}}
{{--                <textarea id="message" name="message" rows="5" class="form-control" required></textarea><br>--}}
{{--                <input type="submit" class="btn btn-primary mt-1" value="Отправить">--}}
{{--                </div>--}}

{{--            </form>--}}

                <p class="mt-3 mb-5 fs-5">Буду рад ответить на ваши вопросы и поделиться опытом. До скорой встречи!</p>
        </div>

    </div>
</x-body>
