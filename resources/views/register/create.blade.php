<x-head></x-head>
<x-body>
    <x-navbar></x-navbar>
    <div class="container mt-5">

        <div class="img-thumbnail text-center border-0">
            <img src="{{asset('img/register-icon.png')}}" alt="auth-icon" width="19%">
        </div>

            <form method="POST" action="/register">
                @csrf
                <div class="mb-3 w-25 m-auto">
                    <label for="name" class="mt-3">Username</label>
                    <input value="{{old('name')}}" type="text" class="form-control" id="name" name="name" placeholder="John Cena">
                    @error('name')
                    <p class="text-danger mt-3">{{$message}}</p>
                    @enderror

                    <label for="email" class="mt-3">Email адрес</label>
                    <input value="{{old('email')}}" type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="example@mail.com">
                    @error('email')
                    <p class="text-danger mt-3">{{$message}}</p>
                    @enderror

                    <label for="password" class="mt-3">Пароль</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Ваш пароль">
                    @error('password')
                    <p class="text-danger mt-3">{{$message}}</p>
                    @enderror

                    <label for="password_confirmation" class="mt-3">Подтвердите пароль</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Ваш пароль">
                    @error('password_confirmation')
                    <p class="text-danger mt-3">{{$message}}</p>
                    @enderror

                    <button type="submit" class="btn btn-primary mt-3">Зарегистрироваться</button>
                </div>
            </form>

    </div>
</x-body>
