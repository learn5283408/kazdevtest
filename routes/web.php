<?php

use App\Http\Controllers\PostCommentsController;
use App\Http\Controllers\SessionsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/yandex_92d0b098de0fee75.html', function () {
    return 
'<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>Verification: 92d0b098de0fee75</body>
</html>'
;
});

Route::get('/googleaa7e7b911d34b86e.html', function () {
    return 
'google-site-verification: googleaa7e7b911d34b86e.html'
;
});

Route::get('/', [PostController::class, 'index']);
Route::get('/posts/{post:id}', [PostController::class, 'show']);
Route::post('/posts/{post:id}/comments', [PostCommentsController::class, 'store']);

Route::get('/manual', function () {
    return view('manual');
});

Route::get('/automation', function () {
    return view('automation');
});

Route::get('/contacts', function () {
    return view('contacts');
});


Route::get('/register', [RegisterController::class, 'create'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store'])->middleware('guest');

Route::get('/login', [SessionsController::class, 'create'])->middleware('guest')->name('login');
Route::post('/login', [SessionsController::class, 'store'])->middleware('guest');
Route::post('/logout', [SessionsController::class, 'destroy'])->middleware('auth');


Route::middleware('can:admin')->group(function () {
    Route::get('/admin/posts', [AdminController::class, 'index'])->middleware('admin');
    Route::get('/admin/posts/{post}/edit', [AdminController::class, 'edit'])->middleware('admin');
    Route::patch('/admin/posts/{post}', [AdminController::class, 'update'])->middleware('admin');
    Route::delete('/admin/posts/{post}', [AdminController::class, 'destroy'])->middleware('admin');
    Route::get('/admin/posts/create', [AdminController::class, 'create'])->middleware('admin');
    Route::post('/admin/posts', [AdminController::class, 'store'])->middleware('admin');
});
