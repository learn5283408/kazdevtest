<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (!DB::table('tags')->exists()){
            DB::table('tags')->insert([
                ['name' => 'Теория тестирования', 'slug' => 'theory'],
                ['name' => 'HTML', 'slug' => 'html'],
                ['name' => 'CSS', 'slug' => 'css'],
                ['name' => 'JavaScript', 'slug' => 'js'],
                ['name' => 'Автоматизация', 'slug' => 'automation'],
                ['name' => 'Postman', 'slug' => 'postman'],
                ['name' => 'SQL', 'slug' => 'mysql'],
                ['name' => 'Docker', 'slug' => 'docker'],
                ['name' => 'CI/CD', 'slug' => 'cicd'],
                ['name' => 'Тестовые артефакты', 'slug' => 'artifacts']
            ]);
        }
    }
}
